# Traefik

Traefik is a project for launching [Traefik](https://docs.traefik.io/) using
[Docker Compose](https://docs.docker.com/compose/).

# Installation

[Install Docker Compose](https://docs.docker.com/compose/install/).

# Running

```shell
docker-compose up
```
